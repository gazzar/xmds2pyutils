import os

from distutils.core import setup

exec(open('xmds2pyutils/version.py').read())
with open('README.rst', 'r') as f:
    long_description = f.read()

setup(
    name='Xmds2PyUtils',
    version=__version__,
    description='Python helper modules and general Xmds2 helper scripts for use with xmds2',
    long_description=long_description,
    url='https://bitbucket.org/gazzar/xmds2pyutils',
    author='Gary Ruben',
    author_email='gary.ruben@gmail.com',
    packages=['xmds2pyutils'],
    install_requires=[
        'numpy',
        'matplotlib',
        'h5py',
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
    ],
    license='Modified BSD',
    scripts=['xmds2pyutils/scripts/output-field.py'],
)

