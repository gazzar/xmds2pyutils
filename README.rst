Xmds2PyUtils
=========================================================

Python helper module and scripts for use with XMDS2 http://www.xmds.org/.
General helper utilities 2d and 3d order parameter fields in my xmds2
simulations. I am developing these to help in the conversion of the
simulations I originally made for my PhD work which used xmds and which I
am porting to use xmds2.

Features
--------
- Helpers for 2d and 3d multicomponent fields
- Modified BSD License
- Order parameter field operations
- 2d plotting

Requirements
------------
`Xmds2PyUtils`_ is written in Python 2.7 and depends on numpy, matplotlib, h5py.

Installation
------------

$ sudo apt-get update
$ sudo apt-get -y install python-pip
$ sudo apt-get -y install git
$ sudo pip install git+https://bitbucket.org/gazzar/xmds2pyutils


Notes
-----
This version developed and tested on ubuntu 12.10 64-bit

Version History
---------------
0.1     Initial version