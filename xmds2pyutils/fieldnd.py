#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import numpy as np
import hdf5_helpers


"""xmds2pyutils 2d and 3d field helpers"""


class Field(object):
    """A convenience class representing an xmds2 output group defined as

    <output format="hdf5" filename="field">
        <group>
            <sampling basis="x y" initial_sample="no">
            <moments>phiR phiI</moments>
            <dependencies>wavefunction</dependencies>
            <![CDATA[
                phiR = phi.Re();
                phiI = phi.Im();
            ]]>
            </sampling>
        </group>
    </output>

    or for 3d, 
            <sampling basis="x y z" initial_sample="no">

    """
    def __init__(self, filename='', field_name='phi', field=None):
        """The constructor reads an hdf5 file and computes the
        probability density and phase immediately so we only have to
        do this once. Then we can subsequently just read the results.
        The field can also be specified directly via the field parameter.

        Keyword arguments:
        filename - an hdf5 filename. Example 'field.h5'
        field_name - string representing the static portion of the field name. (default 'phi')
        field - An optional 4-tuple (rr, ri, xs, ys) or 5-tuple (rr, ri, xs, ys, zs) that may
                be used to initialise the field instead of reading it from the hdf5 file,
                where rr and ri are n x n arrays and xs, ys, and zs are coordinate n-vectors.
                If field is specified, filename is ignored.

        """
        self.name = filename
        self.field_name = field_name
        if field is None:
            rr, ri, xs, ys = hdf5_helpers.read_from_hdf5(
                                filename, ['1/{}R'.format(field_name),
                                '1/{}I'.format(field_name), '1/x', '1/y'])
        else:
            if len(field) == 4:
                rr, ri, xs, ys = field
            else:
                assert len(field) == 5
                rr, ri, xs, ys, zs = field

        self.rr, self.ri, self.xs, self.ys = rr, ri, xs, ys

        self.x_lattice = xs.size
        self.x_domains = (xs[0], xs[-1])
        self.a_x = np.sqrt(float(self.x_domains[1]-self.x_domains[0]) / self.x_lattice)
        self.y_lattice = ys.size
        self.y_domains = (ys[0], ys[-1])
        self.a_y = np.sqrt(float(self.y_domains[1]-self.y_domains[0]) / self.y_lattice)
        
        self._prob_dens = (rr*rr + ri*ri) * (self.a_x * self.a_y)**2

        # Deal with 3d case
        self.zs = None                          # Change this iff Field is 3d
        if field is None:
            if '1/z' in hdf5_helpers.members(filename):
                zs = hdf5_helpers.read_from_hdf5(filename, '1/z')
                self.zs = zs
        elif len(field) == 5:
            self.zs = zs
        if self.zs is not None:
            self.z_lattice = zs.size
            self.z_domains = (zs[0], zs[-1])
            self.a_z = np.sqrt(float(self.z_domains[1]-self.z_domains[0]) / self.z_lattice)
            self._prob_dens *= self.a_z**2

        self._phase = np.arctan2(ri, rr)

    @property
    def amplitude(self):
        ampl = (self.rr + 1j*self.ri) * self.a_x * self.a_y
        if self.zs is not None:
            ampl *= self.a_z
        return ampl

    def __str__(self):
        return self.amplitude.__str__()

    @property
    def conj(self):
        return self.rr - 1j*self.ri

    @property
    def phase(self):
        return self._phase

    @property
    def abs(self):
        return np.sqrt(self._prob_dens)

    @property
    def prob_dens(self):
        return self._prob_dens

    @phase.setter
    def phase(self, new_phase):
        field = (self.rr + 1j*self.ri) * np.exp(1j * (new_phase - self._phase))
        self.rr = field.real
        self.ri = field.imag
        self._phase = np.arctan2(self.ri, self.rr)

    def write(self, filename):
        f = self.field_name
        hdf5_helpers.write_to_hdf5(filename, {'1/{}R'.format(f) : self.rr,
                                              '1/{}I'.format(f) : self.ri,
                                              '1/x' : self.xs,
                                              '1/y' : self.ys})
        if '1/z' in hdf5_helpers.members(filename):
            hdf5_helpers.write_to_hdf5(filename, {'1/z': self.zs})

    def to_ascii(self, filename):
        """Write the real and imag field parts and their coordinates
        to ascii files with _re.txt, _im.txt, _xs.txt, _ys.txt, _zs.txt appended.

        Keyword arguments:
        filename - an hdf5 filename. Example: 'field.h5'

        """
        rr, ri, xs, ys = self.rr, self.ri, self.xs, self.ys
        np.savetxt('{}_re.txt'.format(filename), rr, fmt='%1.8g')
        np.savetxt('{}_im.txt'.format(filename), ri, fmt='%1.8g')
        np.savetxt('{}_xs.txt'.format(filename), xs, fmt='%1.8g')
        np.savetxt('{}_ys.txt'.format(filename), ys, fmt='%1.8g')
        if '1/z' in hdf5_helpers.members(filename):
            zs = self.zs
            np.savetxt('{}_zs.txt'.format(filename), zs, fmt='%1.8g')


def get_fields(filename, field_parts=['phiR', 'phiI']):
    """Returns real and imag field parts 1/phiR, 1/phiI from an hdf5 file.

    Keyword arguments:
    filename - an hdf5 filename. Example: 'field.h5'
    field_parts - list of real-valued field parts to return (default ['phiR', 'phiI'])

    Returns:
    The specified field parts.

    """
    return hdf5_helpers.read_from_hdf5(filename, ['1/'+part for part in field_parts])


def get_ranges(filename, dim_names=['x','y']):
    """Returns field coordinates in group '1' from an hdf5 file.

    Keyword arguments:
    filename - an hdf5 filename. Example: 'field.h5'
    dim_names - list of coordinate arrays return (default ['x','y'])

    Returns:
    The specified dimensions.

    """
    return hdf5_helpers.read_from_hdf5(filename, ['1/'+dim for dim in dim_names])


if __name__ == '__main__':
    phi = Field(r'tests/testdata/test_4x4.h5')
    print phi.prob_dens.shape
    print phi.phase.shape
