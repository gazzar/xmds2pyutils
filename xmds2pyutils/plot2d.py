#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import numpy as np
from fieldnd import Field
import matplotlib.pyplot as plt
import scipy.ndimage as nd
from ndarray_utils import crop_array


"""xmds2pyutils 2d plot utilities for fields"""


def imsave_density(filename, field, cmap='gray', vmax=None, crop=None):
    """Write the density of a field2d.Field2d to an image file.

    Keyword arguments:
    filename - Image filename including extension. Example: density.png
    field - 2d fieldnd.Field object
    cmap - The matplotlib colour map to use (default 'gray')
    vmax - The vmax value for the matplotlib imsave function (default None)
           By default, leaving vmax unchanged autoscales the max intensity
    crop - See ndarray_utils.crop_array(crop) docstring:
           int -px to +side-length or float -1.0-1.0

    """
    im = field.prob_dens
    im = crop_array(im, crop)
    plt.imsave(filename, im, cmap=cmap, vmin=0.0, vmax=vmax, dpi=1)


def imsave_phase(filename, field, cmap='hsv', cmap_range=None,
                 cmap_threshold=None, crop=None):
    """Write the phase of a field2d.Field2d to an image file.

    Keyword arguments:
    filename - Image filename including extension. Example: phase.png
    field - A 2d fieldnd.Field object
    cmap - The matplotlib colour map to use (default 'hsv')
    cmap_range - Two-element list of floats between 0.0-1.0 for scaling the
        cmap passed through to the rescale_cmap() low and high parameters.
    cmap_threshold - The phase is zeroed where the density falls below this
        fraction of the density max - passed through to the
        threshold_field() threshold parameter.
    crop - See ndarray_utils.crop_array(crop) docstring:
           int -px to +side-length or float -1.0-1.0

    """
    if cmap_range is not None:
        assert len(cmap_range) == 2
        low, high = cmap_range
        assert low < high
        cmap = rescale_cmap(cmap, low, high)

    if cmap_threshold is not None:
        field = threshold_field(field)
    im = crop_array(field.phase, crop)
    plt.imsave(filename, im, cmap=cmap, vmin=-np.pi, vmax=np.pi, dpi=1)


def threshold_field(field, threshold=1e-3, drape=5):
    """Returns a new field based on field whose phase is set to 0 where
    its density falls below the threshold. Often we aren't interested in the
    phase, which can fluctuate wildly in regions of low density, so for display
    purposes we like to set the phase to zero in these regions. In vortical
    fields, we only want to affect the outer tails (think of the parts at the
    base of a volcano) and not the vortex cores, where the field is also low.
    Therefore we overlay the field with a structured maximum filter for
    identifying the tails.

    Keyword arguments:
    field - A 2d fieldnd.Field object
    threshold - fraction of the maximum field density below which field is
        set to zero (default 1e-2)
    drape - Drapes a structured maximum filter of size drape x drape
        over the intensity data, thus avoiding affecting local field minima
        such as vortex core regions (default 5)

    Returns:
    field2d.Field2d object

    """
    import copy

    field = copy.deepcopy(field)
    draped = nd.maximum_filter(field.prob_dens, size=drape)
    field.phase[draped < draped.max()*threshold] = 0.0

    return field


def rescale_cmap(cmap, low=0.0, high=1.0):
    """Returns a colour map with y0, y1 values rescaled linearly from a range 0->1
    to a new range low->high.
    This is mainly useful for phase plots, where the default hsv colours are too saturated.
    The default low and high parameters leave the colours unchanged.

    Keyword arguments:
    cmap - one of the standard matplotlib colormaps. Example: 'hsv'
    low - new low limit (default 0.0)
    high - new high limit (default 1.0)

    Returns:
    A matplotlib colormap

    Example 1:
    # equivalent scaling to mpmath cplot_like(blah, l_bias=0.33, int_exponent=0.0)
    my_hsv = rescale_cmap('hsv', low=0.5)
    Example 2:
    my_hsv = rescale_cmap(cm.hsv, low=0.2, high=0.9)

    """
    from matplotlib import _cm, colors

    # Get cmap data as a dictionary
    if type(cmap) != str:
        cmap = cmap.name
    cm = _cm.datad[cmap]

    # Convert the r, g, b nested (x, y0, y1) tuples to arrays so we can rescale y0 and y1
    r = np.array(cm['red'])
    g = np.array(cm['green'])
    b = np.array(cm['blue'])

    # Rescale
    span = high - low
    r[:, 1:] = r[:, 1:]*span + low
    g[:, 1:] = g[:, 1:]*span + low
    b[:, 1:] = b[:, 1:]*span + low
    rows_to_tuples = lambda rows: [tuple(row) for row in rows]
    _my_data = {'red':   rows_to_tuples(r),
                'green': rows_to_tuples(g),
                'blue':  rows_to_tuples(b)}

    # Make new table from rescaled data
    LUTSIZE = plt.rcParams['image.lut']     # Number of colours to use in lookup table
    my_cmap = colors.LinearSegmentedColormap('my_cmap', _my_data, LUTSIZE)

    return my_cmap


def hls_phase_plot(field, int_exponent=0.6, s=0.8, l_bias=1.0, drape=5):
    """Implements an mpmath.cplot-like colour mapping where phase maps along the
    h-channel and field density map along the l-channel in hls colour space.
    Takes a 2D image array in hls colourspace then transforms to rgb.
    Note that the phase to hue mapping has a pi rotation compared with mpmath.cplot which
    maintains matplotlib's default hsv colour mapping of the phase values.
    The returned image is an field.phase.shape x 3 array, so you need to use matplotlib's
    imshow() to display it.

    Keyword arguments:
    field - A fieldnd.Field object
    int_exponent - Setting to 1.0 applies the intens mask directly to the hls lightness-channel
        mpmath cplot uses 0.3 (default 0.6)
    s - saturation (default is the same as mpmath cplot 0.8)
    l_bias - biases the mean lightness value away from 0.5. mpmath uses 1.0.
        Examples are: l_bias=2 -> mean=0.33 (ie darker), l_bias=0.5 -> mean=0.66 (lighter)
    drape - Drapes a structured maximum filter of size drape x drape
        over the intensity data, thus avoiding affecting local field minima
        such as vortex core regions (default 5)

    """
    from colorsys import hls_to_rgb

    scaled_dens = nd.maximum_filter(field.prob_dens, size=drape)/field.prob_dens.max()

    h = ((field.phase+np.pi) / (2*np.pi)) % 1.0

    l = 1.0 - l_bias/(l_bias + scaled_dens**int_exponent)
    v_hls_to_rgb = np.vectorize(hls_to_rgb)

    return np.dstack(v_hls_to_rgb(h,l,s))


def density_and_phase(field, show=True, threshold=True):
    """Display the density and phase parts of a field2d.Field2d in a matplotlib plot.

    Keyword arguments:
    field - A 2d fieldnd.Field object
    show - Set True to call the matplotlib show() function (default True)
    threshold - Apply the threshold_field() function (default True)

    """
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    from matplotlib.ticker import ScalarFormatter

    if threshold:
        field = threshold_field(field)

    xs, ys = field.xs, field.ys

    pa_cmap = plt.cm.pink               # @UndefinedVariable
    ph_cmap = plt.cm.hsv                # @UndefinedVariable
    TICK_DIRN = 'inout'
    plt.rc("figure", figsize=(12,5))
    plt.rc("figure.subplot", wspace=0.3)
    plt.rc("xtick", direction=TICK_DIRN)
    plt.rc("ytick", direction=TICK_DIRN)

    _, (ax1, ax2) = plt.subplots(1, 2)

    # colorbar formatting
    ti = ScalarFormatter()
    ti.set_scientific(True)
    ti.set_powerlimits((-3, 4))

    # density plot
    ax1.set_title('density')
    im1 = ax1.imshow(field.prob_dens,
        origin='bottom', extent=(xs[0], xs[-1], ys[0], ys[-1]), cmap=pa_cmap)
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", size="5%", pad="2%")
    plt.colorbar(im1, cax=cax, format=ti)

    # phase plot
    ax2.set_title('phase')
    im2 = ax2.imshow(field.phase,
        origin='bottom', extent=(xs[0], xs[-1], ys[0], ys[-1]), cmap=ph_cmap)
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad="2%")
    plt.colorbar(im2, cax=cax, format=ti)
    im2.set_clim(-np.pi, np.pi)

    if show:
        plt.show()


if __name__ == '__main__':
    a = np.ones((6,8,10))
    print a.shape
    b = crop_array(a, [2,0.9,-2])
    print b.shape

    phi = Field(r'tests/testdata/test_4x4.h5')
    density_and_phase(phi)
    plt.matshow(phi.phase, cmap=rescale_cmap('hsv', low=0.2, high=0.9))
    plt.imshow(hls_phase_plot(phi, int_exponent=.5), origin='lower')
    plt.show()
