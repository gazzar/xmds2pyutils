#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import numpy as np
from numpy import rollaxis
from collections import Iterable


"""xmds2pyutils nd array utilities"""


def crop_array(array, crops):
    """Crops an array by removing border pixels.
    This is useful when we have a damping function like a supergaussian in the
    outer simulation region, which is effectively outside the area of interest.

    Keyword arguments:
    crops - An int or float, applied to all axes of the array
            e.g. 0.8
            OR
            A list of ints or floats, one for each axis of the array,
            e.g. [-0.2, 0.8, 100], interpreted as follows:

            If a float in the range 0.0-1.0, defined in terms of the original length.
            If an int, represents an absolute number of pixels.
            If positive, defines the final side length of the result (assumed square).
            If negative, defines the length removed from the array (assumed square).
            Final size is always forced to be an even number.
            Examples: 0.5 returns an array 50% of the original side length.
                     -0.2 returns an array 80% of the original side length.
                      100 returns an array of side length 100px.
                      -20 returns an array with 20px removed from each side.

    Returns:
    cropped ndarray

    """
    def even(n):
        return int(round(n)) & (~1)

    array = array.copy()
    dimensions = len(array.shape)
    if not isinstance(crops, Iterable):
        crops = [crops] * dimensions
    assert len(crops) == dimensions
    for i, crop in enumerate(crops):
        axis_len = array.shape[i]
        if crop is None:
            continue
        elif type(crop) is float:
            assert abs(crop) <= 1.0
            if crop < 0.0:
                faxis_len = even(-crop * axis_len) / 2
                array = rollaxis(rollaxis(array, i)[faxis_len:axis_len-faxis_len], 0, i+1)
            else:
                crop = even(crop * axis_len)
                crop_low = (axis_len-crop) / 2
                crop_high = crop_low + crop
                array = rollaxis(rollaxis(array, i)[crop_low:crop_high], 0, i+1)
        elif type(crop) is int:
            if crop < 0:
                crop = -crop
                array = rollaxis(rollaxis(array, i)[crop:axis_len-crop], 0, i+1)
            else:
                assert crop <= axis_len
                crop = even(crop)
                crop_low = (axis_len-crop) / 2
                crop_high = crop_low + crop
                array = rollaxis(rollaxis(array, i)[crop_low:crop_high], 0, i+1)
        else:
            raise TypeError('crop ({}) must be int or float'.format(type(crop)))

    return array


if __name__ == '__main__':
    from fieldnd import Field

    phi = Field(r'tests/testdata/test_4x4x4.h5')
    a = np.ones((6,8,10))
    print a.shape
    b = crop_array(a, [2,0.9,-2])
    print b.shape
    b = crop_array(a, -2)
    print b.shape
