#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import glob
from plot2d import rescale_cmap, hls_phase_plot


def recolour_images():
    print cmap

    files = glob.glob(glob_pattern)
    for f in files:
        im = plt.imread(f)
        if len(im.shape) == 3:
            # if input files are RGB or RGBA
            im = im[...,0]
        filename = '{0}_{2}{1}'.format(*os.path.splitext(f), cmap)
        plt.imsave(filename, im, cmap=rescale_cmap('hsv',low=0.3,high=0.9), vmin=0, vmax=1)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Applies a new colour map to specified greyscale image files')
    parser.add_argument('glob_pattern', help='input file glob pattern (default %(default)s)', type=str, default='*_ph.png')
    parser.add_argument('cmap', type=str, help='bleh', metavar='CMAP', choices=plt.cm.datad.keys())

    args = parser.parse_args()

    glob_pattern = args.glob_pattern
    cmap = args.cmap

    recolour_images()