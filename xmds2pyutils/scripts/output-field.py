#!/usr/bin/env python

# output field routines
# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import numpy as np
from xmds2pyutils.fieldnd import Field
from xmds2pyutils import plot2d
import matplotlib.pyplot as plt


def output_field():
    """Reads the hdf5 file named filename.h5 which contains the four arrays output
    by xmds: phiR, phiI, x, y.
    phiR and phiI are the 2D real and imag parts of the phi (probability amplitude)
    field, coordinatised by the 1D arrays x and y.
    It computes the probability density and phase and outputs this in a variety of
    formats or simply displays it.

    """
    field = Field(filename, field_name)

    if show_stats:
        print 'N =', field.prob_dens.sum()

    if write_ascii is not None:
        field.to_ascii(write_ascii)

    if im_dens_filename is not None:
        plot2d.imsave_density(im_dens_filename, field,
                              cmap=prob_dens_cmap, vmax=prob_dens_max,
                              crop=crop)

    if im_phase_filename is not None:
        plot2d.imsave_phase(im_phase_filename, field, cmap=phase_cmap,
                            cmap_range=phase_cmap_range,
                            cmap_threshold=phase_cmap_threshold,
                            crop=crop)

    if show_plot:
        plot2d.density_and_phase(field)


if __name__ == "__main__":
    import argparse
    import ast

    parser = argparse.ArgumentParser(description='Output contents of an hdf5 file containing 2D field data')

    parser.add_argument('filename', metavar='infile', type=str,
                        help='Input filename')
    parser.add_argument('-op', '--order-param',
                        help='Order parameter name (default: phi)',
                        type=str, dest='field_name', metavar='NAME', default='phi')

    parser.add_argument('-s', '--stats',
                        help='Show selected statistics',
                        action='store_true', dest='show_stats')
    parser.add_argument('-p', '--plot',
                        help='Show plot',
                        action='store_true', dest='show_plot')
    parser.add_argument('-a', '--ascii',
                        help='Write array as ascii',
                        type=str, dest='write_ascii', metavar='FILENAME', default=None)

    parser.add_argument('-do', '--density-output',
                        help='Save density image',
                        type=str, dest='im_dens_filename', metavar='FILENAME', default=None)
    parser.add_argument('-dc', '--density-cmap', default='gray',
                        help='Density image colour map (default: %(default)s)',
                        type=str, dest='prob_dens_cmap', metavar='CMAP', choices=plt.cm.datad.keys())
    parser.add_argument('-dm', '--density-max',
                        help='Use max_value for scaling values in density image instead of autoscaling',
                        type=float, dest='prob_dens_max', metavar='MAX_VALUE', default=None)

    parser.add_argument('-po', '--phase-output',
                        help='Save phase image',
                        type=str, dest='im_phase_filename', metavar='FILENAME', default=None)
    parser.add_argument('-pc', '--phase-cmap', default='hsv',
                        help='Phase image colour map (default: %(default)s)',
                        type=str, dest='phase_cmap', metavar='CMAP', choices=plt.cm.datad.keys())
    parser.add_argument('-pr', '--phase-cmap-range', default=None,
                        help='Phase colour map scaling range, example: 0.2 0.8 (default: 0.0 1.0)',
                        type=float, nargs=2, dest='phase_cmap_range', metavar='RANGE')
    parser.add_argument('-pt', '--phase-threshold',
                        help='Phase colour map threshold, example 1e-3',
                        type=float, dest='phase_cmap_threshold', metavar='FRACTION', default=None)

    parser.add_argument('-c', '--crop',
                        help='Image crop. See ndarray_utils.crop_array(crop) docstring: int -px to +side-length or float -1.0-1.0 (default: None)',
                        type=str, dest='crop', metavar='VALUE', default=None)

    args = parser.parse_args()

    filename = args.filename
    field_name = args.field_name

    show_stats = args.show_stats
    show_plot = args.show_plot
    write_ascii = args.write_ascii

    im_dens_filename = args.im_dens_filename
    prob_dens_cmap = args.prob_dens_cmap
    prob_dens_max = args.prob_dens_max

    im_phase_filename = args.im_phase_filename
    phase_cmap = args.phase_cmap
    phase_cmap_range = args.phase_cmap_range
    phase_cmap_threshold = args.phase_cmap_threshold

    crop = None if args.crop is None else ast.literal_eval(args.crop)

    output_field()
