#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import numpy as np
from fieldnd import Field
import scipy.ndimage as nd
from scipy.fftpack import diff


"""xmds2pyutils field operations"""


def threshold_field(field, threshold=1e-3, drape=5):
    """Returns a new field based on field whose phase is set to 0 where
    its density falls below the threshold. Often we aren't interested in the
    phase, which can fluctuate wildly in regions of low density, so for display
    purposes we like to set the phase to zero in these regions. In vortical
    fields, we only want to affect the outer tails (think of the parts at the
    base of a volcano) and not the vortex cores, where the field is also low.
    Therefore we overlay the field with a structured maximum filter for
    identifying the tails.

    Keyword arguments:
    field - field2d.Field2d object
    threshold - fraction of the maximum field density below which field is
        set to zero (default 1e-2)
    drape - Drapes a structured maximum filter of size drape x drape
        over the intensity data, thus avoiding affecting local field minima
        such as vortex core regions (default 5)

    Returns:
    fieldnd.Field object

    """
    import copy

    field = copy.deepcopy(field)
    draped = nd.maximum_filter(field.prob_dens, size=drape)
    field.phase[draped < draped.max()*threshold] = 0.0

    return field


def grad(field):
    """grad of the field
    2d: grad(f) = df/dx i_ + df/dy j_ where d's are partials
    3d: grad(f) = df/dx i_ + df/dy j_ + df/dz k_ where d's are partials

    Keyword arguments:
    field - fieldnd.Field object

    Returns:
    A tuple containing i_, j_ (and k_ if 3d) components respectively

    """
    f = field.rr + 1j*field.ri

    assert 2 <= len(f.shape) <= 3

    if len(f.shape) == 2:
        df_dx, df_dy = np.gradient(f)
        return df_dx, df_dy
    elif len(f.shape) == 3:
        df_dx, df_dy, df_dz = np.gradient(f)
        return df_dx, df_dy, df_dz


def curl(fi, fj, fk):
    """curl of the field f = fi i_ + fj j_ + fk k_
    curl(f) =  (dfk/dy - dfj/dz) i_ + (dfi/dz - dfk/dx) j_ + (dfj/dx - dfi/dy) k_ where d's are partials

    Keyword arguments:
    fi, fj, fk - 3d vector field components

    Returns:
    A tuple containing the resulting i_, j_ and k_ components.

    """
    assert len(fi.shape) == len(fj.shape) == len(fk.shape) == 3

    dfi_dx, dfi_dy, dfi_dz = np.gradient(fi)
    dfj_dx, dfj_dy, dfj_dz = np.gradient(fj)
    dfk_dx, dfk_dy, dfk_dz = np.gradient(fk)

    return (dfk_dy-dfj_dz, dfi_dz-dfk_dx, dfj_dx-dfi_dy)


def vorticity(psi):
    """Vorticity of the field psi

    Keyword arguments:
    psi - fieldnd.Field object

    Returns:
    The vorticity curl(v_), where v_ = j_/|psi|^2

    """
    dpsi_dx, dpsi_dy, dpsi_dz = grad(psi)

    # calculate the probability current components
    jx = (psi.conj * dpsi_dx).imag
    del dpsi_dx
    jy = (psi.conj * dpsi_dy).imag
    del dpsi_dy
    jz = (psi.conj * dpsi_dz).imag
    del dpsi_dz

    # the velocity components from prob. currents
    vx = jx / psi.prob_dens
    del jx
    vy = jy / psi.prob_dens
    del jy
    vz = jz / psi.prob_dens
    del jz

    return curl(vx, vy, vz)


def weighted_vorticity(psi):
    """Vorticity measure of the field psi - this is similar to
    vorticity but differs in 

    Keyword arguments:
    psi - fieldnd.Field object

    Returns:
    curl(j_/|psi|)

    """
    dpsi_dx, dpsi_dy, dpsi_dz = grad(psi)

    # calculate the probability current components
    jx = (psi.conj * dpsi_dx).imag
    del dpsi_dx
    jy = (psi.conj * dpsi_dy).imag
    del dpsi_dy
    jz = (psi.conj * dpsi_dz).imag
    del dpsi_dz

    # the prob. current components weighted by the local density.
    # cf. vorticity velocity components
    wx = jx / psi.abs
    del jx
    wy = jy / psi.abs
    del jy
    wz = jz / psi.abs
    del jz

    return curl(wx, wy, wz)


if __name__ == '__main__':
    phi = Field(r'tests/testdata/test_4x4x4.h5')
    print phi

    phiconj = phi.conj
    dphi_dx, dphi_dy, dphi_dz = grad(phi)
    jx = (phiconj * dphi_dx).imag
    jy = (phiconj * dphi_dy).imag
    jz = (phiconj * dphi_dz).imag
    print curl(jx, jy, jz)
