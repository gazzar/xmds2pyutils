#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import numpy as np
from numpy import arccos, sin, cos, exp, abs, angle
from fieldnd import Field
from field_operations import grad, curl, vorticity
from mayavi import mlab, modules
import matplotlib.pyplot as plt
import scipy.ndimage as nd
import plot2d
import os


"""xmds2pyutils 3d plot utilities for fields"""


def vortices_from_vorticity(field, colour=(0.5,0.5,0.5), threshold=0.3):
    """Show the vortices by getting the field vorticity and isosurfacing it.

    Keyword arguments:
    field - A 3d fieldnd.Field object

    """
    curlw = vorticity(field)
    norm = np.sqrt(curlw[0]**2 + curlw[1]**2 + curlw[2]**2)
    t = norm.min() + (norm.max()-norm.min())*threshold
    mlab.contour3d(norm, color=colour, contours=[t])


def isosurface(field, contour=0.1, colour=(.4,.6,.8), opacity=1.0, background_colour=(1,1,1)):
    """Single isosurface of the field.prob_dens.

    Keyword arguments:
    field - A 3d fieldnd.Field object.
    contour - The contour value of field.prob_dens.
    colour - an RGB tuple for the isosurface [default (.4,.6,.8)].
             Note, a good complement to this is (.9,.2,.2).
    opacity - alpha of isosurface (default 1.0).
    background_colour - an RGB tuple [default (1,1,1)=white].

    """
    mlab.gcf().scene.background = background_colour
    mlab.contour3d(field.prob_dens, contours=[contour], color=colour, opacity=opacity)


def volume(field, gradient="", background_colour=(0,0,0)):
    """Volumetric visualization of the field.prob_dens.

    Keyword arguments:
    field - A 3d fieldnd.Field object.
    gradient - A vtk .grad gradient colourmap name, e.g. 'blue.grad' (default "").
    background_colour - an RGB tuple [default (0,0,0)=black].

    """
    mlab.gcf().scene.background = background_colour

    src = mlab.pipeline.scalar_field(field.prob_dens)
    vol = mlab.pipeline.volume(src)
    if gradient != "":
        gradient_path = None
        try:
            open(gradient)
            gradient_path = gradient
        except IOError:
            try:
                local_gradient_path = os.path.join(os.path.dirname(__file__), 'colourmaps', gradient)
                open(local_gradient_path)
                gradient_path = local_gradient_path
            except IOError:
                print '{} not found'.format(gradient)
        if gradient_path is not None:
            modules.volume.load_volume_prop_from_grad(gradient_path, vol.volume_property, (0,field.prob_dens.max()))
    vol.volume_property.diffuse = 0.99


def cutplane(field, axis=2):
    """Show the field.prob_dens projected along a field axis to an image file.

    Keyword arguments:
    field - A 3d fieldnd.Field object
    axis - 0-2, where 0:x-axis, 1:y-axis, 2:z-axis (default=2:z-axis)

    """
    plane_orientation = {0:'x_axes', 1:'y_axes', 2:'z_axes'}[axis]
    side = field.rr.shape[axis]

    src = mlab.pipeline.scalar_field(field.prob_dens)
    mlab.pipeline.image_plane_widget(src, plane_orientation=plane_orientation)

def cut_octant(field, contour=0.1, background_colour=(1,1,1)):
    """Isosurface of the field.prob_dens with an octant removed.

    Keyword arguments:
    field - A 3d fieldnd.Field object.
    contour - The contour value of field.prob_dens.
    background_colour - an RGB tuple [default (1,1,1)=white].

    """
    azimuth = 45
    mlab.gcf().scene.background = background_colour

    xlen,ylen,zlen = field.rr.shape
    src1 = mlab.pipeline.scalar_field(field.prob_dens)

    # set up three overlapping half-volumes
    grid1 = mlab.pipeline.extract_grid(src1)
    grid1.filter.voi = [0,xlen/2.,0,ylen,0,zlen]
    grid2 = mlab.pipeline.extract_grid(src1)
    grid2.filter.voi = [0,xlen,0,ylen/2.,0,zlen]
    grid3 = mlab.pipeline.extract_grid(src1)
    grid3.filter.voi = [0,xlen,0,ylen,0,zlen/2.]

    # surface contours
    contour1 = mlab.pipeline.contour(grid1)
    contour2 = mlab.pipeline.contour(grid2)
    contour3 = mlab.pipeline.contour(grid3)
    contour1.filter.contours = contour2.filter.contours = contour3.filter.contours = [contour]
    mlab.pipeline.surface(contour1)
    mlab.pipeline.surface(contour2)
    mlab.pipeline.surface(contour3)

    # cutplanes
    plane1 = mlab.pipeline.scalar_cut_plane(grid1, plane_orientation='x_axes')
    plane2 = mlab.pipeline.scalar_cut_plane(grid2, plane_orientation='y_axes')
    plane3 = mlab.pipeline.scalar_cut_plane(grid3, plane_orientation='z_axes')
    plane1.enable_contours = plane2.enable_contours = plane3.enable_contours = True
    plane1.contour.filled_contours = plane2.contour.filled_contours = plane3.contour.filled_contours = True
    plane1.implicit_plane.widget.enabled = plane2.implicit_plane.widget.enabled = plane3.implicit_plane.widget.enabled = False
    plane1.implicit_plane.origin = plane2.implicit_plane.origin = plane3.implicit_plane.origin = (xlen/2.,ylen/2.,zlen/2.)
    plane1.contour.minimum_contour = plane2.contour.minimum_contour = plane3.contour.minimum_contour = contour


def bloch_vectors(field1, field2, mode=1, decimate=4, background_colour=(1,1,1)):
    """Taking two field components as bases, show Bloch vectors, scaled by local density.

    Keyword arguments:
    field1, field2 - Two 3d fieldnd.Field objects.
    mode = 0=2d arrows, 1=3d arrows (default=1).
    decimate = integer factor (e.g. 2 or 4) dividing number of field points to render (default=4).
    background_colour - an RGB tuple [default (1,1,1)=white].

    """
    psi1 = field1.amplitude
    psi2 = field2.amplitude

    ## Just pull out the z=0 slice
    ## psi1 = psi1[...,zlen/2][...,np.newaxis]
    ## psi2 = psi2[...,zlen/2][...,np.newaxis]

    # normalise psi
    norm = np.hypot(abs(psi1), abs(psi2))
    psi1, psi2 =  (psi1, psi2) / norm

    xi = angle(psi1)                        # find the local phase xi so we can remove it
    psi1, psi2 = exp(-1j*xi) * (psi1, psi2) # now rotate psi0 and psi1 by -xi to make the first coefficient real

    theta = 2*arccos(psi1.real)             # this gives us theta
    phi = angle(psi2 / sin(theta/2))        # which gives us phi
    phi[np.isnan(phi)] = 0                  # deal with the poles i.e. where theta=0 or pi

    # Now we've got our angles and norm, get the Bloch sphere arrow components
    u = norm * sin(theta) * cos(phi)
    v = norm * sin(theta) * sin(phi)
    w = norm * cos(theta)

    mlab.figure(bgcolor=background_colour)
    xlen,ylen,zlen = field1.rr.shape
    x, y, z = np.mgrid[1:xlen:xlen*1j, 1:ylen:ylen*1j, 1:zlen:zlen*1j]
    ## x, y, z = np.mgrid[1:xlen:xlen*1j, 1:ylen:ylen*1j, 1::1j]
    mode = ['2darrow', 'arrow'][mode]
    colormap = ['jet', 'Blues', 'winter', 'copper', 'cool', 'blue-red'][0]
    q = mlab.quiver3d(x, y, z, u, v, w, mode=mode, colormap=colormap, scale_factor=decimate/10, mask_points=decimate)
    q.glyph.glyph_source.glyph_position = 'center'


def imsave_midplane_phase(filename, field, axis=2, cmap='hsv', cmap_range=None, cmap_threshold=None, crop=None):
    """Write the phase of the middle slice of fieldnd.Field to an image file.

    Keyword arguments:
    filename - Image filename including extension. Example: density.png
    field - A 3d fieldnd.Field object
    axis - 0-2, where 0:x-axis, 1:y-axis, 2:z-axis (default=2:z-axis)
    cmap - The matplotlib colour map to use (default 'hsv')
    cmap_range - Two-element list of floats between 0.0-1.0 for scaling the
        cmap passed through to the rescale_cmap() low and high parameters.
    cmap_threshold - The phase is zeroed where the density falls below this
        fraction of the density max - passed through to the
        threshold_field() threshold parameter.
    crop - See ndarray_utils.crop_array(crop) docstring:
           int -px to +side-length or float -1.0-1.0

    """
    # make a 2d field sliced from the 3d field
    side = field.rr.shape[axis]
    rr = np.rollaxis(field.rr, axis)[side/2]
    ri = np.rollaxis(field.ri, axis)[side/2]
    xs = np.arange(rr.shape[0])
    ys = np.arange(rr.shape[1])
    field2d = Field(field=(rr, ri, xs, ys))

    plot2d.imsave_phase(filename, field2d, cmap, cmap_range, cmap_threshold, crop)


def imsave_projected_density(filename, field, axis=2, cmap='gray', vmax=None, crop=None):
    """Write the field.prob_dens projected along a field axis to an image file.

    Keyword arguments:
    filename - Image filename including extension. Example: density.png
    field - A 3d fieldnd.Field object
    axis - 0-2, where 0:x-axis, 1:y-axis, 2:z-axis (default=2:z-axis)
    cmap - The matplotlib colour map to use (default 'gray')
    vmax - The vmax value for the matplotlib imsave function (default None)
           By default, leaving vmax unchanged autoscales the max intensity
    crop - See ndarray_utils.crop_array(crop) docstring:
           int -px to +side-length or float -1.0-1.0

    """
    im = field.prob_dens.sum(axis=axis)
    im = plot2d.crop_array(im, crop)
    plt.imsave(filename, im, cmap=cmap, vmin=0.0, vmax=vmax, dpi=1)


if __name__ == '__main__':
    import scipy.signal as ss

    SIDE = 50
    ampl = np.ones((SIDE,SIDE,SIDE))
    z = np.zeros((SIDE,SIDE,SIDE))
    THR = 4
    z[:THR,:THR,:THR] = np.random.normal(size=(THR,THR,THR))
    ph = np.angle(np.fft.fftn(z))
    z = ampl * np.exp(1j*ph)
    rr = z.real
    ri = z.imag
    xs = np.linspace(-10,10,SIDE)
    ys = np.linspace(-10,10,SIDE)
    zs = np.linspace(-10,10,SIDE)

    psi = Field(field=(rr, ri, xs, ys, zs))
    vortices_from_vorticity(psi, colour=(.5,.8,.5))
    #~ isosurface(psi)
    #~ cutplane(psi)
    #~ cut_octant(psi)
    mlab.show()
