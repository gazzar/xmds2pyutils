#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import h5py
import numpy as np


"""xmds2pyutils hdf5 i/o routines"""


def members(filename):
    """Retrieve a list of all members of an hdf5 file.

    Keyword arguments:
    filename - an hdf5 filename. Example 'field.h5'

    """
    mems = []
    with h5py.File(filename, 'r') as f:
        f.visit(mems.append)
    return mems


def write_to_hdf5(filename, sources_and_dests):
    """Write a dictionary of label:content pairs to an hdf5 file.

    Keyword arguments:
    filename - an hdf5 filename. Example 'field.h5'
    sources_and_dests - a dictionary of the form {'dest1':source1, 'dest2':source2, ...}

    """
    with h5py.File(filename,'w') as f:
        for key, val in sources_and_dests.iteritems():
            f[key] = val


def read_from_hdf5(filename, source):
    """Retrieve a list containing the specified members of an hdf5 file.

    Keyword arguments:
    filename - an hdf5 filename. Example 'field.h5'
    source - a string or list of strings used to lookup the items:
        Example 1: 'item1'
        Example 2: 'group1/item2'
        Example 3: ['item3', 'group2/item3']

    Returns:
    The item or a list of items.

    """
    def read_source(source):
        a = f[source]
        dest = np.empty(a.shape)
        f[source].read_direct(dest)
        return dest

    with h5py.File(filename, 'r') as f:
        if isinstance(source, (list, tuple)):
            return [read_source(path) for path in source]
        else:
            return read_source(source)


if __name__ == '__main__':
    f = r'tests/testdata/test_4x4.h5'
    print members(f)
    print read_from_hdf5(f, '1/x')
    print read_from_hdf5(f, ['1/x', '1/y'])
