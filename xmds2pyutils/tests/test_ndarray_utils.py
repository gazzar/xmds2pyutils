#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import sys, os
PATH_HERE = os.path.abspath(os.path.dirname(__file__))
sys.path = [os.path.join(PATH_HERE, '..')] + sys.path
import unittest
import ndarray_utils as nu
import numpy as np


class Crop2dArrayTests(unittest.TestCase):
    def setUp(self):
        self.array = np.arange(30**2).reshape(30, 30)

    def test_crop_type(self):
        self.assertRaises(TypeError, nu.crop_array, (self.array, '20'))
        self.assertTrue(isinstance(nu.crop_array(self.array, 20), np.ndarray))
        self.assertTrue(isinstance(nu.crop_array(self.array, -20), np.ndarray))
        self.assertTrue(isinstance(nu.crop_array(self.array, 0.8), np.ndarray))
        self.assertTrue(isinstance(nu.crop_array(self.array, -0.2), np.ndarray))

    def test_pos_int(self):
        result = nu.crop_array(self.array, 29)
        self.assertTrue(result.shape == (28,28))
        self.assertTrue(result[0, 0] == 31)
        result = nu.crop_array(self.array, 2)
        self.assertTrue(result.shape == (2,2))

    def test_even_size_output(self):
        result = nu.crop_array(self.array, 29)
        self.assertTrue(result.shape == (28,28))
        result = nu.crop_array(self.array, 28)
        self.assertTrue(result.shape == (28,28))
        result = nu.crop_array(self.array, 7)
        self.assertTrue(result.shape == (6,6))

    def test_neg_int(self):
        result = nu.crop_array(self.array, -1)
        self.assertTrue(result.shape == (28,28))
        self.assertTrue(result[0, 0] == 31)

    def test_pos_float(self):
        result = nu.crop_array(self.array, 1.0)
        self.assertTrue(result.shape == (30,30))
        self.assertTrue(result[0, 0] == 0)
        result = nu.crop_array(self.array, 0.95)
        self.assertTrue(result.shape == (28,28))
        self.assertTrue(result[0, 0] == 31)
        result = nu.crop_array(self.array, 0.8)
        self.assertTrue(result.shape == (24,24))

    def test_neg_float(self):
        result = nu.crop_array(self.array, -0.5)
        self.assertTrue(result.shape == (16,16))
        result = nu.crop_array(self.array, -0.1)
        self.assertTrue(result.shape == (28,28))
        self.assertTrue(result[0, 0] == 31)


class Crop3dArrayTests(unittest.TestCase):
    def setUp(self):
        self.array = np.arange(4*6*8).reshape(4, 6, 8)

    def test_crop_type(self):
        self.assertRaises(TypeError, nu.crop_array, (self.array, '20'))
        self.assertTrue(isinstance(nu.crop_array(self.array, 2), np.ndarray))
        self.assertTrue(isinstance(nu.crop_array(self.array, -2), np.ndarray))
        self.assertTrue(isinstance(nu.crop_array(self.array, 0.8), np.ndarray))
        self.assertTrue(isinstance(nu.crop_array(self.array, -0.2), np.ndarray))

    def test_pos_int(self):
        result = nu.crop_array(self.array, 2)
        self.assertTrue(result.shape == (2, 2, 2))
        self.assertTrue(result[0, 0, 0] == 67)
        result = nu.crop_array(self.array, [2, 4, 6])
        self.assertTrue(result.shape == (2, 4, 6))


if __name__ == '__main__':
    import nose2
    nose2.main()