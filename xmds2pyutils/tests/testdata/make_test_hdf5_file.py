#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

"""Makes a small test hdf5 file with the same general structure as a field output by xmds2"""

import h5py
import numpy as np


SIZE = 4
filename = 'test_4x4.h5'
with h5py.File(filename, 'w') as f:
    dset = f.create_dataset('1/x', (SIZE,), dtype='f8')
    dset[...] = np.arange(SIZE, dtype=np.float32)

    dset = f.create_dataset('1/y', (SIZE,), dtype='f8')
    dset[...] = np.arange(SIZE, 2*SIZE, dtype=np.float32)

    dset = f.create_dataset('1/phiI', (SIZE,SIZE), dtype='f8')
    dset[...] = np.arange(SIZE*SIZE, dtype=np.float32).reshape((SIZE,SIZE))

    dset = f.create_dataset('1/phiR', (SIZE,SIZE), dtype='f8')
    dset[...] = np.arange(SIZE*SIZE, 0, -1, dtype=np.float32).reshape((SIZE,SIZE))

filename = 'test_4x4x4.h5'
with h5py.File(filename, 'w') as f:
    dset = f.create_dataset('1/x', (SIZE,), dtype='f8')
    dset[...] = np.arange(SIZE, dtype=np.float32)

    dset = f.create_dataset('1/y', (SIZE,), dtype='f8')
    dset[...] = np.arange(SIZE, 2*SIZE, dtype=np.float32)

    dset = f.create_dataset('1/z', (SIZE,), dtype='f8')
    dset[...] = np.arange(2*SIZE, 3*SIZE, dtype=np.float32)

    dset = f.create_dataset('1/phiI', (SIZE,SIZE,SIZE), dtype='f8')
    dset[...] = np.arange(SIZE*SIZE*SIZE, dtype=np.float32).reshape((SIZE,SIZE,SIZE))

    dset = f.create_dataset('1/phiR', (SIZE,SIZE,SIZE), dtype='f8')
    dset[...] = np.arange(SIZE*SIZE*SIZE, 0, -1, dtype=np.float32).reshape((SIZE,SIZE,SIZE))
