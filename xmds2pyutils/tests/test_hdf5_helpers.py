#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import sys, os
PATH_HERE = os.path.abspath(os.path.dirname(__file__))
sys.path = [os.path.join(PATH_HERE, '..')] + sys.path
import unittest
import hdf5_helpers


TESTDATA_DIR = os.path.join(PATH_HERE, 'testdata')
SIZE = 4


class DatasetLoadTests(unittest.TestCase):
    def setUp(self):
        self.filename = os.path.join(TESTDATA_DIR, 'test_4x4.h5')

    def test_members(self):
        filename = self.filename
        self.assertTrue(u'1' in hdf5_helpers.members(filename))
        self.assertTrue(u'1/phiI' in hdf5_helpers.members(filename))
        self.assertTrue(u'1/phiR' in hdf5_helpers.members(filename))
        self.assertTrue(u'1/x' in hdf5_helpers.members(filename))
        self.assertTrue(u'1/y' in hdf5_helpers.members(filename))

    def test_read_x_from_hdf5(self):
        x = hdf5_helpers.read_from_hdf5(self.filename, '1/x')
        self.assertEqual(x.size, SIZE)
        
    def test_read_xy_from_hdf5(self):
        x, y = hdf5_helpers.read_from_hdf5(self.filename, ['1/x', '1/y'])
        self.assertEqual(x.size, SIZE)
        self.assertEqual(y.size, SIZE)

    def test_read_phiR_from_hdf5(self):
        filename = self.filename
        x = hdf5_helpers.read_from_hdf5(filename, '1/phiR')
        self.assertEqual(x.shape, (SIZE,SIZE))


if __name__ == '__main__':
    import nose2
    nose2.main()