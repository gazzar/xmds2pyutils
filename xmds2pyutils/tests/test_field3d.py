#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import sys, os
PATH_HERE = os.path.abspath(os.path.dirname(__file__))
sys.path = [os.path.join(PATH_HERE, '..')] + sys.path
import unittest
import fieldnd
import numpy as np


TESTDATA_DIR = os.path.join(PATH_HERE, 'testdata')
SIZE = 4


class CorrectLoadTests(unittest.TestCase):
    def setUp(self):
        filename = os.path.join(TESTDATA_DIR, 'test_4x4x4.h5')
        self.field = fieldnd.Field(filename)

    def test_simple_load(self):
        self.assertTrue(isinstance(self.field, fieldnd.Field))

    def test_phase_shape(self):
        self.assertEqual(self.field.phase.shape, (SIZE,SIZE,SIZE))

    def test_density_shape(self):
        self.assertEqual(self.field.prob_dens.shape, (SIZE,SIZE,SIZE))


class FunctionTests(unittest.TestCase):
    def setUp(self):
        self.filename = os.path.join(TESTDATA_DIR, 'test_4x4x4.h5')

    def test_get_field(self):
        phiR = fieldnd.get_fields(self.filename, field_parts=['phiR'])[0]
        self.assertEqual(phiR.shape, (SIZE,SIZE,SIZE))

    def test_get_fields(self):
        phiR, phiI = fieldnd.get_fields(self.filename)
        self.assertEqual(phiR.shape, (SIZE,SIZE,SIZE))
        self.assertEqual(phiI.shape, (SIZE,SIZE,SIZE))

    def test_get_ranges(self):
        x, y = fieldnd.get_ranges(self.filename)
        self.assertEqual(x.shape, (SIZE,))
        self.assertEqual(y.shape, (SIZE,))


class PhaseSettingTests(unittest.TestCase):
    def setUp(self):
        filename = os.path.join(TESTDATA_DIR, 'test_4x4x4.h5')
        self.field = fieldnd.Field(filename)

    def test_set_and_verify(self):
        density = self.field.prob_dens
        phase = self.field.phase
        self.field.phase = 0.0
        self.assertTrue(np.allclose(density, self.field.prob_dens))
        self.assertTrue(np.allclose(self.field.phase, np.zeros_like(phase)))
        self.field.phase = 1.0
        self.assertTrue(np.allclose(self.field.phase, np.ones_like(phase)))
        # test branch cut at [-pi, pi]
        self.field.phase = 3 * np.pi / 2
        self.assertTrue(np.allclose(self.field.phase, -np.pi/2*np.ones_like(phase)))


class InitialiseTests(unittest.TestCase):
    def setUp(self):
        rr = np.linspace(0,999,1000).reshape(10,10,10)
        ri = np.linspace(1000,1999,1000).reshape(10,10,10)
        xs = np.linspace(-50,50,100)
        ys = np.linspace(-50,50,100)
        zs = np.linspace(-50,50,100)
        self.rr, self.ri, self.xs, self.ys, self.zs = rr, ri, xs, ys, zs
        self.field = fieldnd.Field(field=(rr,ri,xs,ys,zs))

    def test_init_correctly(self):
        self.assertTrue(np.allclose(self.field.rr, self.rr))
        self.assertTrue(np.allclose(self.field.ri, self.ri))
        self.assertTrue(np.allclose(self.field.xs, self.xs))
        self.assertTrue(np.allclose(self.field.ys, self.ys))
        self.assertTrue(np.allclose(self.field.zs, self.zs))


if __name__ == '__main__':
    import nose2
    nose2.main()